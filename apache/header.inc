<!DOCTYPE html>
<html>
	<head>
		<title>KDE - Experience Freedom!</title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="description" content="KDE download">

		<link rel="shortcut icon" href="//cdn.kde.org/img/favicon.ico">
		<link rel="stylesheet" media="screen" type="text/css" href="//cdn.kde.org/aether-devel/bootstrap.css">
	</head>

	<body>
		<header class="header">
			<nav class="navbar">
				<a class="kde-logo navbar-brand active" href="/"></a>
				<ul class="navbar-nav mr-auto ml-3">
					<li class="nav-item"><a href="/?mirrorstats" class="nav-link">Mirrorstats</span></a>
				</ul>
			</nav>
		</header>
		<div class="container">
