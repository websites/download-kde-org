   </div> <!-- /pageRow -->

   <footer id="kFooter" class="footer">
        <section id="kLinks" class="container pb-4">
            <div class="row">
                <nav class="col-sm">
                    <h3>Products</h3>
                    <a href="https://kde.org/plasma-desktop" hreflang="">Plasma</a>
                    <a href="https://apps.kde.org" hreflang="">KDE Applications</a>
                    <a href="https://develop.kde.org/products/frameworks/">KDE Frameworks</a>
                    <a href="https://plasma-mobile.org" hreflang="">Plasma Mobile</a>
                    <a href="https://neon.kde.org/">KDE Neon</a>
                </nav>

                <nav class="col-sm">
                    <h3>Develop</h3>
                    <a href="https://api.kde.org/">API Documentation</a>
                    <a href="https://doc.qt.io/" rel="noopener" target="_blank">Qt Documentation</a>
                    <a href="https://inqlude.org/" rel="noopener" target="_blank">Inqlude Documentation</a>
                    <a href="https://kde.org/goals">KDE Goals</a>
                    <a href="https://invent.kde.org/">Source code</a>
                </nav>

                <nav class="col-sm">
                    <h3>News &amp; Press</h3>
                    <a href="https://kde.org/announcements/" hreflang="">Announcements</a>
                    <a href="https://dot.kde.org/">KDE.news</a>
                    <a href="https://planet.kde.org/">Planet KDE</a>
                    <a href="https://kde.org/contact/">Press Contacts</a>
                    <a href="https://kde.org/stuff">Miscellaneous Stuff</a>
                    <a href="https://kde.org/thanks">Thanks</a>
                </nav>

                <nav class="col-sm">
                    <h3>Resources</h3>
                    <a href="https://community.kde.org/Main_Page">Community Wiki</a>
                    <a href="https://kde.org/support/">Help</a>
                    <a href="https://kde.org/download/">Download KDE Software</a>
                    <a href="https://kde.org/code-of-conduct/">Code of Conduct</a>
                    <a href="https://kde.org/privacypolicy">Privacy Policy</a>
                    <a href="https://kde.org/privacypolicy-apps">Applications Privacy Policy</a>
                </nav>

                <nav class="col-sm">
                    <h3>Destinations</h3>
                    <a href="https://store.kde.org/">KDE Store</a>
                    <a href="https://ev.kde.org/">KDE e.V.</a>
                    <a href="https://kde.org/community/whatiskde/kdefreeqtfoundation">KDE Free Qt Foundation</a>
                    <a href="https://timeline.kde.org" hreflang="">KDE Timeline</a>
                    <a href="https://manifesto.kde.org" hreflang="">KDE Manifesto</a>
                    <a href="https://kde.org/support/international/">International Websites</a>
                </nav>
            </div>
        </section>

        <div id="kSocial" class="container kSocialLinks">
            <a class="shareMatrix" href="https://go.kde.org/matrix/#/#kde:kde.org" aria-label="Share on Matrix"></a>
            <a class="shareFacebook" href="https://www.facebook.com/kde/" aria-label="Post on Facebook"></a>
            <a class="shareTwitter" href="https://twitter.com/kdecommunity" aria-label="Share on Twitter"></a>
            <a class="shareMastodon" href="https://floss.social/@kde" rel="me" aria-label="Share on Mastodon"></a>
            <a class="shareLinkedIn" href="https://www.linkedin.com/company/29561/" aria-label="Share on LinkedIn"></a>
            <a class="shareReddit" href="https://www.reddit.com/r/kde/" aria-label="Share on Reddit"></a>
            <a class="shareYouTube" href="https://www.youtube.com/channel/UCF3I1gf7GcbmAb0mR6vxkZQ" aria-label="Share on YouTube"></a>
            <a class="sharePeerTube" href="https://tube.kockatoo.org/a/kde_community/video-channels" aria-label="Share on Peertube"></a>
            <a class="shareVK" href="https://vk.com/kde_ru" aria-label="Share on VK"></a>
            <a class="shareInstagram" href="https://www.instagram.com/kdecommunity/" aria-label="Share on Instagram"></a>
        </div>

        <div id="kLegal" class="container">
            <p class="d-block d-xl-flex flex-wrap mb-0 py-3 h6">
                <small lang="en" class="maintenance">
                    Maintained by <a href="mailto:kde-www@kde.org">KDE Webmasters</a> (public mailing list).
                </small>
                <small class="trademark">
                    KDE<sup>®</sup> and <a href="https://kde.org/media/images/trademark_kde_gear_black_logo.png">the K Desktop Environment<sup>®</sup> logo</a>
                    are registered trademarks of <a href="https://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> |
                    <a href="https://kde.org/community/whatiskde/impressum">Legal</a>
                </small>
            </p>
        </div>
    </footer>
<script>
function wrap(el, wrapper) {
  el.parentNode.insertBefore(wrapper, el);
  wrapper.appendChild(el);
}
document.querySelectorAll('table').forEach(table => {
  const wrapElement = document.createElement('div');
  wrapElement.classList.add('card');
  wrapElement.classList.add('my-4');
  wrap(table, wrapElement);
  table.classList.add('table');
  table.classList.add('card-body');
  table.classList.add('mb-0');
  table.classList.add('table-striped');
});
document.querySelectorAll('table tr hr').forEach(hr => hr.parentNode.parentNode.remove())
document.querySelectorAll('[alt="[   ]"]').forEach(hr => {
  const tr = hr.parentNode.parentNode.parentNode;
  const href = hr.parentNode.href; //.getAttributes('href');
  const description = tr.lastChild;
  const element = document.createElement('a');
  element.innerHTML = "Details";
  element.href = href + '.mirrorlist';
  description.appendChild(element);
});

// Insert a license header for Akademy if needed
if( document.location.host == 'files.kde.org' && document.location.pathname.startsWith('/akademy') ) {
  // Locate our parent container
  var container = document.querySelector('div.container')

  // Make a container for ourselves
  var messageBox = document.createElement('div')
  messageBox.classList.add('card', 'my-4', 'alert', 'alert-info');

  // Determine the resource we should fetch
  var messageFileToDisplay = document.location.href + ".message";

  // Populate it's contents...
  fetch( messageFileToDisplay )
  .then( (response) => {
    // If we receive an error then just do nothing
    if (!response.ok) {
      throw new Error(`HTTP error: ${response.status}`);
    }

    // Otherwise request the text for use in the next phase
    return response.text();
  })

  // Process the received text
  .then((text) => {
    // By setting it on the container for the message...
    messageBox.textContent = text;

    // And then making that visible
    container.insertBefore(messageBox, container.children[0]);
  });
}

</script>
  </body>
</html>
